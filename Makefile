PYTHON = python3.6

# Directories
OUT_DIR = bin
SRC_DIR = src
EXP_DIR = experiments
REPORT_DIR = report
POSTER_DIR = poster

LIBS=lib:src

POSTER = poster
REPORT = report
MAIN = main
QLEARN = qlearn
QAC = qac

.PHONY: run clean build write-report plot

submit: build
	@mkdir $(OUT_DIR)/code
	@rsync -azP --exclude=".git*" --exclude-from=.gitignore . $(OUT_DIR)/code
	@tar -cz -C $(OUT_DIR) $(REPORT).pdf code -f $(OUT_DIR)/projeto-4.tar.gz
	@rm -rf $(OUT_DIR)/code

build: $(OUT_DIR)/$(REPORT).pdf

clean:
	@find . -name "*.pyc" -delete
	@rm -rf $(OUT_DIR)/* $(LOG_DIR)/*

run_qac:
	@mkdir -p $(OUT_DIR) $(LOG_DIR) $(EXP_DIR)
	@PYTHONPATH=$(LIBS) $(PYTHON) $(SRC_DIR)/$(QAC)/$(MAIN).py -o $(OUT_DIR) -e $(EXP_DIR)

run_qlearn:
	@mkdir -p $(OUT_DIR) $(LOG_DIR) $(EXP_DIR)
	@PYTHONPATH=$(LIBS) $(PYTHON) $(SRC_DIR)/$(QLEARN)/$(MAIN).py -o $(OUT_DIR) -e $(EXP_DIR)

eval_qlearn:
ifndef ID
	$(error ID is undefined)
endif
	@mkdir -p $(OUT_DIR)
	@PYTHONPATH=$(LIBS) $(PYTHON) $(SRC_DIR)/$(QLEARN)/$(MAIN).py -e $(EXP_DIR) --eval $(ID)

resume_qlearn:
ifndef ID
	$(error ID is undefined)
endif
	@mkdir -p $(OUT_DIR)
	@PYTHONPATH=$(LIBS) $(PYTHON) $(SRC_DIR)/$(QLEARN)/$(MAIN).py -e $(EXP_DIR) --resume $(ID)

plot:
ifndef ID
	$(error ID is undefined)
endif
	@PYTHONPATH=$(LIBS) $(PYTHON) $(SRC_DIR)/parselogs.py --exp_dir $(EXP_DIR) --id $(ID)

write-report: $(REPORT_DIR)/$(REPORT).tex
	latexmk -pdf -pvc -cd $< -aux-directory=$(OUT_DIR) -output-directory=$(OUT_DIR)

$(OUT_DIR)/$(REPORT).pdf: $(REPORT_DIR)/$(REPORT).tex
	latexmk -pdf -cd $< -aux-directory=$(OUT_DIR) -output-directory=$(OUT_DIR)
