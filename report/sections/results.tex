\section{Resultados e Discussão}\label{sec:results}
  O retorno total $G_0$ médio entre 50 episódios obtido durante o treinamento com o conjunto de ações $\mathcal{A}_{PID}$ é dado pela Figura~\ref{fig:pid-returns}, onde as barras de erro representam um intervalo de confiança de 95\% da média. Como esperado, o retorno tende a aumentar durante o treinamento, conforme o robô aproxima-se de coletar o \emph{puck}. As barras de erro também têm um comportamento esperado, visto que no início a variância em $G_0$ é baixa, já que o robô praticamente só toma ações aleatórias e acaba perdendo o \emph{puck} de visão rapidamente. Em torno do episódio 300, o Robotino já aprende a pegar o \emph{puck} algumas vezes, mas o $\epsilon$ ainda é muito alto, fazendo com que a variância no retorno seja extremamente alta. Ao aproximar-se do final do treinamento, o robô pega o \emph{puck} a maioria das vezes e a variância volta a diminuir. Note que no gráfico, $G_0$ não inclui $R_T$, o retorno obtido pelo robô ao atingir um estado terminal.

  Devido à modelagem da recompensa, o retorno não é uma boa métrica para sabermos o quão boa é uma solução. O robô poderia tomar inúmeras ações desnecessárias que não prejudicariam a soma das recompensas obtidas, como ir para frente e para trás, obtendo umas recompensa total nula. Por isso, o gráfico da Figura~\ref{fig:pid-steps} ajuda-nos a avaliar o quão boa é a política sendo aprendida, já que espera-se que em uma política ótima, o número de passos que o robô leva para chegar ao estado objetivo seja mínimo. Como esperado, o gráfico é extremamente ruidoso no início, quando o robô pega o \emph{puck} por um acaso, mas torna-se mais consistente com o tempo, a medida que o $\epsilon$ diminui e a política é aprendida.

  \begin{figure}[!ht]
    \centering
    \subfloat[]{
      \label{fig:pid-returns}
      \quad\includegraphics[width=\textwidth]{./images/pid/returns.pdf}
    }\\
    \subfloat[]{
      \label{fig:pid-steps}
      \quad\includegraphics[width=\textwidth]{./images/pid/steps.pdf}
    }
    \caption{Retorno total médio a cada 50 episódios com intervalo de confiança de 95\% para o experimento feito com o conjunto de ações $\mathcal{A}_{PID}$~\protect\subref{fig:pid-returns}. Abaixo, temos o número de passos tomados para pegar o \emph{puck} nos episódios em que o robô foi bem-sucedido~\protect\subref{fig:pid-steps}}\label{fig:pid-graphs}
  \end{figure}

  A capacidade do robô de generalizar para outras posições iniciais pode ser visualizada a partir do mapa de estados visitados, dado pela Figura~\ref{fig:pid-heatmap}, onde os estados com cores mais claras foram visitados mais frequentemente. Note como a maior parte do mapa, está preenchida, sinal que a taxa de exploração com decaimento exponencial foi eficaz em explorar os estados mesmo que a posição inicial do robô tenha mantido-se fixa. Os estados mais visitados concentram-se na parte superior da imagem, sinal que o robô procura primeiro ajustar sua orientação para alinhar-se com o \emph{puck} antes de mover para frente. Uma mapa melhor distribuído poderia ser obtido se o robô fosse inicializado em posições aleatórias, mas escolhemos guardar essa inicialização para a fase de inferência em que queríamos avaliar a capacidade de generalização do robô.

  \begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{./images/pid/states-map.pdf}
    \caption{Estados visitados durante o treinamento do robô com o conjunto de ações $\mathcal{A}_{PID}$, onde uma cor mais clara indica que o estado foi visitado mais vezes e os estados em preto não foram visitados.}\label{fig:pid-heatmap}
  \end{figure}

  O mapa de ações dado pela Figura~\ref{fig:pid-actions} usa uma seta para representar a ação ótima de cada estado considerando uma política gulosa. Note como as primerias fileiras da imagem e os dois semi-círculos na parte inferior têm setas que só apontam para cima, pois essa é a ação padrão e esses estados não foram visitados. Em geral, espera-se que os estados no centro da imagem tenham uma seta orientada para cima, indicando que quando o centroide do \emph{puck} estiver no centro, o robô deve mover-se para frente. Quando o centroide estiver na parte esquerda da imagem, as setas devem apontar mais para esquerda, indicando que o robô deve realizar esse movimento para alinhar-se com o centroide do \emph{puck}. Note como os estados no canto superior direito têm uma seta apontando para direita e para baixo, indicando que o robô deve mover-se para direita girando no sentido horário. Em geral, o mapa apresenta setas com as orientações esperadas, mas existem inúmeros estados com uma ação ótima incoerente, principalmente nas partes central direita e esquerda da imagem, que são os estados pouco visitados. Espera-se que com um tempo maior de treinamento e uma inicialização aleatória da posição inicial, o robô aprendesse uma tabela bem mais coerente.

  \begin{figure}[!th]
    \centering
    \includegraphics[width=\textwidth]{./images/pid/actions-map.pdf}
    \caption{Ação correspondente a $\max_a Q(S, a)$ para cada estado $S$ após o treinamento com o conjunto de ações $\mathcal{A}_{PID}$. As setas representam aproximadamente a direção do movimento induzido pela ação ótima em um dado estado.}\label{fig:pid-actions}
  \end{figure}

  Para o conjunto de ações $\mathcal{A}_{wheel}$, os gráficos do retorno médio por episódio e do número de passos para pegar o \emph{puck} são dados pela Figura~\ref{fig:wheel-graphs}. Note como o retorno médio tende a aumentar em torno do episódio 200 e se estabiliza em torno do retorno máximo no episódio 450. Comparado com o $\mathcal{A}_{PID}$, podemos ver que o $\mathcal{A}_{wheel}$ aprendeu a pegar o \emph{puck} mais rápido, porém foi inconsistente até o final do treinamento. No final, o intervalo de confiança ainda é muito grande, sinal que o robô ainda perdia o \emph{puck} inúmeras vezes e precisava de mais tempo para aprender um solução mais robusta. Isso era de se esperar, visto que o tamanho da tabela $Q(s,a)$ a ser aprendida era cerca de 2,17 vezes maior que para o $\mathcal{A}_{PID}$. No gráfico da Figura~\ref{fig:wheel-steps}, vemos que o conjunto de ações $\mathcal{A}_{wheel}$ inicialmente demora mais de 300 passos para pegar o \emph{puck}, mas rapidamente aprende a completar a tarefa em menos de 100 passos.

  \begin{figure}[!ht]
    \centering
    \subfloat[]{
      \label{fig:wheel-returns}
      \quad\includegraphics[width=\textwidth]{./images/wheel/returns.pdf}
    }\\
    \subfloat[]{
      \label{fig:wheel-steps}
      \quad\includegraphics[width=\textwidth]{./images/wheel/steps.pdf}
    }
    \caption{Retorno total médio a cada 50 episódios com intervalo de confiança de 95\% para o experimento feito com o conjunto de ações $\mathcal{A}_{wheel}$~\protect\subref{fig:wheel-returns}. Abaixo, temos o número de passos tomados para pegar o \emph{puck} nos episódios em que o robô foi bem-sucedido~\protect\subref{fig:wheel-steps}}\label{fig:wheel-graphs}
  \end{figure}

  No mapa de estados visitados para o conjunto de ações $\mathcal{A}_{wheel}$ dado pela Figura~\ref{fig:wheel-heatmap}, temos um trecho significativo que ainda não foi visitado à direita da imagem. Era de esperar que menos estados fossem visitados, visto que o conjunto de ações era maior e uma exploração mais longa era necessária. Porém, um dos fatores que contribuiu para o desempenho insatisfatório da exploração foi o viés do conjunto de ações. Devido à inicialização da tabela $Q$ com 0s e a implementação da função \texttt{np.argmax}, que no caso de empate, retorna o primeiro elemento máximo encontrado, as ações com menor índice sempre eram escolhidas primeiro. Do conjunto de 26 ações, as de menor índice correspondem a um movimento para esquerda com diferentes velocidades angulares. Se o conjunto de ações fosse intercalado com movimentos para esquerda e para direita ou se retornássemos uma ação aleatória no caso de empate, seria possível obter resultados bem melhores na exploração.

  \begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{./images/wheel/states-map.pdf}
    \caption{Estados visitados durante o treinamento do robô com o conjunto de ações $\mathcal{A}_{wheel}$, onde uma cor mais clara indica que o estado foi visitado mais vezes e os estados em preto não foram visitados.}\label{fig:wheel-heatmap}
  \end{figure}

  Terminado o treinamento, foi feita a inferência durante 100 episódios, onde estávamos preocupados com o número de vezes que o robô pegou o \emph{puck} (\emph{i.e.} acurácia) e o número médio de passos que ele levou para pegá-lo. Devido à inicialização aleatória da posição do robô, o retorno máximo possível por episódio varia muito e não seria uma métrica razoável para avaliar o desempenho das soluções. Para tornar o número de passos comparável, aplicou-se a mesma velocidade de rotação sobre as rodas do robô. A Tabela~\ref{tab:metrics} mostra que o conjunto de ações $\mathcal{A}_{PID}$ obteve um desempenho melhor tanto no número de passos médio, na consistência dos resultados (\emph{i.e.} intervalo de confiança menor) e na acurácia em pegar o \emph{puck}. Isso era de se esperar, visto que no treinamento com o conjunto de ações $\mathcal{A}_{wheel}$ o robô não teve tempo suficente para aprender uma solução mais melhor. Espera-se que com mais tempo de treinamento, a solução com o $\mathcal{A}_{wheel}$ obtivesse uma acurácia semelhante ao $\mathcal{A}_{PID}$, mas coletasse o \emph{puck} em um menor número de passos, visto que o maior número de ações permite a realização de um percurso mais refinado.

  \begin{table}[h!]
  \caption{Acurácia e número de passos médios com intervalo de confiança de 95\% para 100 episódios usando uma política gulosa com o robô inicializado em posições e orientações aleatórias}\label{tab:metrics}
  \centering
   \begin{tabular}{|c|c|c|}
   \hline
   \textbf{Ações} & \textbf{Acurácia} & \textbf{Passos}\\ \hline
   $\mathcal{A}_{PID}$ & 95\% & $52,36 \pm 11,52$ \\
   $\mathcal{A}_{wheel}$ & 86\% & $54,43 \pm 14,98$ \\
   \hline
   \end{tabular}
  \end{table}
