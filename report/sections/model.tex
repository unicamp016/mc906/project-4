\section{Modelagem do Problema}\label{sec:model}
	Apesar da simplicidade do problema, muitas das modelagens utilizadas não apresentaram sinais de convergência e foram rapidamente alteradas antes mesmo da coleta de dados do treinamento. Essas modelagens não serão apresentadas na Seção~\ref{sec:results} de resultados, mas serão documentadas nesta seção para evitar a reiteração dos erros em futuros trabalhos. O principal foco do trabalho foi verificar o desempenho do algoritmo de aprendizado Q-Learning, usando abordagens baseadas em modelo e livres de modelo para o conjunto de ações. Apesar do algoritmo apresentar inúmeros hiperparâmetros que poderiam ser tunados para obter um desempenho melhor em cada uma das modelagens, devido à demora significativa dos experimentos usando Aprendizado por Reforço, optou-se por variar componentes maiores na modelagem, como o conjunto de ações utilizado.

	\subsection{Q-Learning}
	O Q-Learning é um algoritmo que busca aprender a função ação-valor $Q$, aproximando-se de $q_*$, a função ação-valor ótima, independente da política sendo seguida (\emph{i.e.} o algoritmo é \emph{off-policy})~\cite{sutton}. A convergência é garantida se todos os pares estado-valor continuam a ser atualizados. O pseudocódigo do Q-Learning é dado pelo Algoritmo~\ref{alg:qlearn}.

	\begin{algorithm}[htbp]
		\caption{Pseudocódigo do Q-Learning}\label{alg:qlearn}
		\begin{algorithmic}
			\State \textbf{Parâmetros}: tamanho do passo $\alpha \in (0, 1]$, pequeno $\epsilon > 0$, número de episódios $N_e$ e número de passos $N_s$.
			\State \textbf{Inicialize}: $Q(s, a)$ arbitrariamente para todo $s \in \mathcal{S}$, $a \in \mathcal{A}(s)$, exceto pelos estados terminais $s_t \in \mathcal{S}_{terminal}$, onde $Q(s_t, a) = 0, \forall a \in \mathcal{A}(s_t)$

			\For {$e \in \mathbf{range}(N_e)$}
				\State Inicialize $S_0$
				\For {$t \in \mathbf{range}(N_s)$}
					\State Escolha $A_t$ a partir de $S_t$ usando a política derivada de $Q$ (\emph{e.g.} $\epsilon$-greedy)
					\State Tome a ação $A_t$, observe $R_{t+1}$, $S_{t+1}$
					\State Atualize a função ação-valor:
					\State ~~~~ $\delta_t = R_{t+1} + \gamma \max_{a} Q(S_{t+1}, a) - Q(S_t, A_t) $
					\State ~~~~$Q(S_t, A_t) \leftarrow Q(S_t, A_t) + \alpha \cdot \delta_t $
					\If {$S_{t+1} \in \mathcal{S}_{terminal}$}
						\State \textbf{break}
					\EndIf
				\EndFor
			\EndFor

		\end{algorithmic}
	\end{algorithm}

	\subsubsection{Estado}

		O Q-Learning requer um conjunto finito de estados e ações para formar a tabela $Q(s, a)$. Para reduzir o número de estados possíveis com uma imagem RGB, optou-se por representar o estado do robô através da posição do centroide do \emph{puck} na imagem. Dessa forma, se a imagem capturada pelo sensor de visão tiver dimensões $M \times N$, o número de estados é $M \cdot N$. Inicialmente, foram feitos vários experimentos usando uma imagem de entrada de dimensões $240 \times 320$, totalizando $76,800$ estados. Considerando que cada episódio tomava uma média de 10 passos e cerca de 10s para completar, um treinamento de 5000 episódios exploraria no máximo $65,1\%$ dos estados e levaria cerca de 14h. Ou seja, a convergência seria extremamente lenta, considerando que o algoritmo deve experimentar inúmeras ações para cada estado, visitando-o múltiplas vezes.

		Aplicando uma redução na dimensionalidade da imagem de entrada para $60 \times 80$, foi possível reduzir o número de estados para 4800, tornando o problema mais tratável.

		O estado deve ser representado por um índice na tabela $Q$. A partir da posição do centroide na imagem, o índice que representa o estado $S$ pode ser obtido aritmeticamente através da Equação~\ref{eq:state}, onde $N$ é o número de colunas na imagem e $(x_c, y_c)$ é a coordenada do centroide do \emph{puck}.

		\begin{equation}\label{eq:state}
			S = x_c + N \cdot y_c
		\end{equation}

	 	O centroide é obtido transformando a imagem para o espaço HSV e aplicando um limiar para filtrar apenas a cor do \emph{puck}. Em seguida, aplica-se o operador morfológico de fechamento com elemento estruturante de dimensões $6 \times 6$. Finalmente, calcula-se o centroide a partir da Equação~\ref{eq:centroid}, onde $m_{pq}$ é o momento geométrico de ordem $p + q$ dado pela Equação~\ref{eq:moments}, e $f(x, y)$ são os níveis de cinza de uma imagem com dimensões $M \times N$.

		\begin{equation}\label{eq:moments}
			m_{pq} = \sum_{x=0}^{M-1}\sum_{y=0}^{N-1} x^p y^q f(x, y)
		\end{equation}

		\begin{equation}\label{eq:centroid}
			x_c = \frac{m_{10}}{m_{00}}~~~\mathrm{e}~~~
			y_c = \frac{m_{01}}{m_{00}}
		\end{equation}

		A Figura~\ref{fig:vision-sensor} ilustra a imagem capturada pelo sensor de visão do Robotino em diferentes resoluções e o centroide em vermelho computado a partir da máscara filtrada no espaço HSV. Na Figura~\ref{fig:centroid}, o centroid encontra-se na posição $(12, 7)$ da imagem, que corresponde ao estado 572.

		\begin{figure}[!ht]
	    \centering
	    \subfloat[$240 \times 320$]{
	      \label{fig:vision-sensor-high}
	      \quad\includegraphics[width=0.45\textwidth]{./images/vision-sensor-high.png}
	    }
	    \subfloat[$60 \times 80$]{
	      \label{fig:vision-sensor-low}
	      \quad\includegraphics[width=0.45\textwidth]{./images/vision-sensor-low.png}
	    }\\
			\subfloat[Centroide]{
				\label{fig:centroid}
				\quad\includegraphics[width=0.45\textwidth]{./images/vision-sensor-low-mask.png}
			}
	    \caption{Sensor de visão do Robotino com resolução $240 \times 320$~\protect\subref{fig:vision-sensor-high} e $60 \times 80$~\protect\subref{fig:vision-sensor-low}. Embaixo, temos a máscara obtida a partir da imagem $60 \times 80$ após a aplicação do filtro no espaço HSV e o centroide do \emph{puck} destacado em vermelho~\protect\subref{fig:centroid}.}\label{fig:vision-sensor}
	  \end{figure}

		% Dar um exemplo da representacao do estado

	\subsubsection{Política}
		O Q-learning busca aprender uma função que estima o quão bom é um agente tomar uma determinada ação em um determinado estado. Essa estimativa é expressa pela soma das recompensas até chegar no estado objetivo (\emph{i.e.} o retorno esperado). Porém, o agente só pode estimar a recompensa dada uma forma particular de agir, definida pela política $\pi$ do agente.

		Para inferência, a política utilizada sempre é uma política gulosa dada pela Equação~\ref{eq:greedy}.

		\begin{equation}\label{eq:greedy}
			A_t = \mathrm{arg} \max_{a} Q(S_{t}, a)
		\end{equation}

		Para o treinamento, é importante balancear a exploração com o aproveitamento (\emph{i.e. exploitation}). Por isso, utilizamos a política $\epsilon$-greedy, onde $\epsilon \in (0, 1]$ é a probabilidade de selecionar uma ação aleatória a partir de um estado. No início do treinamento é interessante usar um $\epsilon \approx 1$, para garantir que o robô explore bem o espaço de estados. Para que o robô consiga completar a tarefa, é importante diminuir a taxa de exploração gradualmente. Isso foi pode ser feito através de um $\epsilon$ que decai exponencialmente, denotado por $\epsilon_e$ e dado pela Equação~\ref{eq:eps-decay}, onde $i$ é o número do episódio, $\epsilon_{max}$ é o valor inicial e máximo do $\epsilon$ e $\epsilon_{min}$ é o valor final e mínimo para o $\epsilon$.

		\begin{equation}\label{eq:eps-decay}
			\epsilon_e = e^{-0,003 \cdot i} (\epsilon_{max} - \epsilon_{min}) + \epsilon_{min}
		\end{equation}

	\subsubsection{Ações}

	A cada passo $t$ do episódio, o agente deve selecionar uma ação $A_t \in \mathcal{A}$ usando a política $\pi$. Neste trabalho, exploramos o efeito de utlizar dois conjuntos diferentes de ações: $\mathcal{A}_{PID}$ e $\mathcal{A}_{wheel}$.

	\begin{itemize}
		\item $\mathbf{\mathcal{A}_{PID}}$:

		Para cada estado, este conjunto permite que sejam tomadas 12 ações distintas. Cada ação corresponde a uma tripla $(v_x, v_y, \omega_z)$, representando velocidade linear nos eixos $x$, $y$ e a velocidade angular em torno do eixo $z$, respectivamente. Essas velocidades são passadas para um controlador PID, que as traduz para $w_i$, o número de rotações por minuto aplicadas à roda $i$. As 12 ações possíveis são $\{(\alpha, 0, k), (-\alpha, 0, k), (0, \alpha, k), (0, -\alpha, k)\}$, em que $k \in \{0, -\rho, \rho\}$, $\alpha = 0,02~\mathrm{m/s}$ e $\rho = 0,1~\mathrm{rad/s}$.

		\item $\mathbf{\mathcal{A}_{wheel}}$:

		Para cada estado, esete conjunto permite com que sejam tomadas 26 ações, no qual as ações possíveis consistem de todas as combinações para uma tripla $(w_1, w_2, w_3)$, em que $w_i \in \{-\beta, 0, +\beta\}$ e $\beta = 0,5~\mathrm{rpm}$, exceto pela tripla $(0, 0, 0)$ que corresponde à ação de ficar parado. Nessa tripla, $w_i$ é o número de rotações por minuto aplicado à roda $i$. Utilizando este conjunto de ações, o agente é livre para determinar o controle adequado para o problema, sem nenhum método de Teoria de Controle.

	\end{itemize}


	\subsubsection{Recompensa}
	O propósito ou objetivo do agente é formalizado em termos de um sinal chamado recompensa, passado do ambiente para o agente. A cada passo $t$ do episódio, o ambiente retorna uma recompensa $R_{t+1}$, dependendo do estado $S_t$ e ação $A_t$ selecionada a partir da política $\pi$. O objetivo do agente é maximizar a recompensa total que ele recebe, isto é, o retorno $G_t$ dado pela Equação~\ref{eq:return}, em que $T$ é o passo limite do episódio, isto é, $T = 1000$, ou $S_T$ é um estado terminal.

	\begin{equation}\label{eq:return}
		G_t = R_{t+1} + R_{t+2} + ... + R_T
	\end{equation}

	A primeira tentativa de modelar a recompensa foi atribuindo uma recompensa negativa proporcional à distância euclideana entre o centroide do \emph{puck} e o objetivo. Essa distância é normalizada pela comprimento da diagonal que atravessa a imagem de forma que a recompensa seja invariante às dimensões da imagem. Ao atingir o estado objetivo, o agente recebe uma recompensa $R_g = +100$ e ao perder o \emph{puck} do campo de visão, o robô recebe uma recompensa $R_l~=~-10^5$. Como a tabela $Q$ é inicializada com 0s e as recompensas são sempre negativas, isso acaba incentivando muito a exploração, já que a melhor ação sempre será uma que ainda não foi tomada, considerando que a estimativa 0 para o valor de $Q(s, a)$ será maior que o valor negativo na célula que já foi visitada.

	Para não se preocupar com a inicialização, a recompensa foi modelada como a diferença entre a distância do objetivo ao centroide do \emph{puck} no passo $t-1$ e $t$. A Equação~\ref{eq:reward} indica a recompensa normalizada para o passo $t$, onde $c_t = ({x_c}_t, {y_c}_t)$ é a posição do centroide do \emph{puck} no passo $t$ e $g = (x_g, y_g)$ é a posição do objetivo.

	\begin{equation}\label{eq:reward}
		R_t = \frac{{||c_{t-1} - g||}_2 - {||c_t - g||}_2}{\sqrt{M^2 + N^2}}
	\end{equation}

	% \subsection{Q-Actor-Critic}
	% 	Os métodos do gradiente de política buscam aprender uma política parametrizada que seleciona uma ação sem consultar a função valor~\cite{sutton}. Apesar da função valor ainda poder ser usada para aprender a política, ele não é necessária para selecionar uma ação. Os métodos \emph{actor-critic} além de aprender uma função valor parametrizada,
	%
	% 	\begin{algorithm}[htbp]
	% 		\caption{Pseudocódigo do Q-Actor-Critic}\label{alg:qac}
	% 		\begin{algorithmic}
	% 			\State \textbf{Parâmetros}: Taxas de aprendizado $\alpha_{\theta}$ e $\alpha_{w}$, número de episódios $N_e$ e número de passos $N_s$.
	% 			\State \textbf{Inicialize}: Os parâmetos $\theta$ e $w$ das redes responsáveis por aprender a política $\pi_{\theta}(a|s)$ e a função ação-valor $Q_{w}(s, a)$, respectivamente.
	%
	% 			\For {$e \in \mathbf{range}(N_e)$}
	% 				\State Inicialize $S_0$
	% 				\State Amostre uma ação $A_0$ usando $\pi_{\theta}(a|S_0)$.
	% 				\For {$t \in \mathbf{range}(N_s)$}
	% 					\State Tome a ação $A_t$, observe $R_{t+1}$, $S_{t+1}$
	% 					\State Amostre uma ação $A_{t+1}$ usando $\pi_{\theta}(a|S_{t+1})$.
	% 					\State Atualize os parâmetros da política:
	% 					\State ~~~~$\theta \leftarrow \theta + \alpha_{\theta} Q_w(S_t, A_t)\nabla_{\theta} \log \pi_{\theta}(a|S_t)$
	% 					\State Atualize os parâmetros da função ação-valor:
	% 					\State ~~~~$\delta_t = R_{t+1} + \gamma Q_w(S_{t+1}, A_{t+1}) - Q_w(S_t, A_t)$
	% 					\State ~~~~$w \leftarrow w + \alpha_w \delta_t \nabla_w Q_w(S_t, A_t)$
	% 					\If {$S_{t+1} \in \mathcal{S}_{terminal}$}
	% 						\State \textbf{break}
	% 					\EndIf
	% 				\EndFor
	% 			\EndFor
	%
	% 		\end{algorithmic}
	% 	\end{algorithm}
