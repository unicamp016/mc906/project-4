import numpy as np

import utils

MAX_LINEAR = 0.02
MAX_ANGULAR = 0.1

# Maps an action in range (0,12) to a velocity setting
pid_action_to_vel = {
    0: (MAX_LINEAR, 0.0,  0.0), # Move up, no steering
    1: (-MAX_LINEAR, 0.0, 0.0), # Move down, no steering
    2: (0.0, MAX_LINEAR, 0.0), # Move left, no steering
    3: (0.0, -MAX_LINEAR, 0.0), # Move right, no steering
    4: (MAX_LINEAR, 0.0,  MAX_ANGULAR), # Move up, steer left
    5: (-MAX_LINEAR, 0.0, MAX_ANGULAR), # Move down, steer left
    6: (0.0, MAX_LINEAR, MAX_ANGULAR), # Move left, steer left
    7: (0.0, -MAX_LINEAR, MAX_ANGULAR), # Move right, steer left
    8: (MAX_LINEAR, 0.0,  -MAX_ANGULAR), # Move up, steer right
    9: (-MAX_LINEAR, 0.0, -MAX_ANGULAR), # Move down, steer right
    10: (0.0, MAX_LINEAR, -MAX_ANGULAR), # Move left, steer right
    11: (0.0, -MAX_LINEAR, -MAX_ANGULAR), # Move right, steer right
}

# Auxiliar dictionary
action_to_rot = {
    0: 0.0,
    1: -0.5,
    2: 0.5
}

# Maps an action in range (0,26) to a velocity setting
wheel_action_to_vel = {}
for i in range(1, 27):
    qt, w1 = divmod(i, 3)
    w2, w3 = divmod(qt, 3)
    wheel_action_to_vel[i - 1] = (action_to_rot[w1], action_to_rot[w2], action_to_rot[w3])

class QLearn:

    def __init__(self, robotino, config):
        self.policy = config['policy']

        self.actions = config['actions']
        if self.actions == "wheel":
            self.actions_dict = wheel_action_to_vel
        elif self.actions == "pid":
            self.actions_dict = pid_action_to_vel
        else:
            raise ValueError("Invalid action taking policy")

        self.alpha = config['alpha']
        self.gamma = config['gamma']

        # Keep track of current step and episode
        self.step = 0
        self.episode = 0
        self.total_episodes = config['episodes']
        self.total_steps = config['steps']
        self.robotino = robotino

        # Helper constants
        self.width, self.height = robotino.resolution
        self.diag = np.sqrt(self.width**2 + self.height**2)

        # Initialize action-state values
        self.Q = np.zeros(shape=(self.height*self.width, len(self.actions_dict)), dtype=np.float64)

        # Scale goal to image resolution
        self.goal_coord = config['goal'][0]*self.width, config['goal'][1]*self.height
        self.goal_state = self.coord_to_state(self.goal_coord)
        self.goal_reward = config['goal_reward']

        # Policy dependent attributes
        self.t = 1
        if self.policy == "eps-greedy":
            eps_high, eps_low = config['epsilon']
            self.epsilon_fn = lambda episode: (np.e**(-0.003*episode))*(eps_high - eps_low) + eps_low
        elif self.policy == "ucb":
            np.seterr(divide='ignore')  # Ignore division by zero errors
            self.c = config['c']
            self.N = np.zeros(shape=(self.height*self.width, len(self.actions_dict)), dtype=np.int64)
        else:
            raise ValueError("Invalid policy selected")

        # Keep track of previous centroid distance to compute reward
        self.prev_dist = None

    def get_state(self):
        img = self.robotino.read_vision_sensor()
        mask = utils.get_mask(img, color='b')
        centroid = utils.get_centroid_coord(mask)

        # In case puck is out of range
        if centroid is None:
            return -1

        state = self.coord_to_state(centroid)
        return state

    def get_action(self, state):
        if self.policy == "ucb":
            action = np.argmax(self.Q[state] + self.c * np.sqrt(np.log(self.t/self.N[state])))
            self.N[state, action] += 1
        elif self.policy == "eps-greedy":
            if np.random.uniform() < self.epsilon_fn(self.episode):
                action = np.random.randint(0, len(self.actions_dict))
            else:
                action = np.argmax(self.Q[state])
        return action

    def take_action(self, action):
        velocities = self.actions_dict[action]
        if self.actions == "pid":
            self.robotino.set_velocity(
                linear_x=velocities[0],
                linear_y=velocities[1],
                angular_z=velocities[2]
            )
        elif self.actions == "wheel":
            self.robotino.set_wheel_velocity(velocities)
        self.robotino.wait(5)
        new_state = self.get_state()
        new_centroid = self.state_to_coord(new_state)

        # Give a positive reward for moving in the direction of the puck
        dist = self.distance(new_centroid, self.goal_coord)/self.diag
        if dist < 0.03:
            reward = self.goal_reward
        else:
            reward = self.prev_dist - dist
        self.prev_dist = dist
        return reward, new_state

    def update_value(self, state, action, reward, new_state):
        update = reward + self.gamma * np.max(self.Q[new_state]) - self.Q[state, action]
        self.Q[state, action] = self.Q[state, action] + self.alpha * update

    def episode_iterator(self, start=0):
        self.episode = start
        self.robotino.start_sim()
        self.prev_dist = self.distance(self.state_to_coord(self.get_state()), self.goal_coord)/self.diag
        while self.episode < self.total_episodes:
            yield self.episode
            self.episode += 1
            self.step = 0
            self.robotino.start_sim()
            self.prev_dist = self.distance(self.state_to_coord(self.get_state()), self.goal_coord)/self.diag

    def step_iterator(self):
        while self.step < self.total_steps:
            yield self.step
            self.t += 1     # Used for UCB action selection
            self.step += 1

    def coord_to_state(self, coord):
        x, y = coord
        return y * self.width + x

    def state_to_coord(self, state):
        y = state // self.width
        x = state % self.width
        return x, y

    def distance(self, point1, point2):
        return np.sqrt((point2[0]-point1[0])**2 + (point2[1]-point1[1])**2)

    def load(self, filename):
        self.Q = np.load(filename)

    def save(self, filename):
        return np.save(filename, self.Q)
