import os
import glob
import argparse
import traceback
import subprocess

import cv2
import numpy as np

import robot
import qlearn
import utils

ACTION_STATE_VALUES_FN = 'action-state-values.npy'

config = {
    "alpha": 0.2,
    "gamma": 0.99,
    "steps": 1000,
    "episodes": 2000,
    "policy": "eps-greedy",
    "epsilon": [0.9, 0.01],
    "actions": "pid",  # "wheel" or "pid"
    "c": 2,
    "goal": (0.5125, 0.95), # (x, y) normalized coordinates for goal
    "goal_reward": 100
}

def main(robotino, ql, log_f1, log_f2, run_dir, eval, resume, show=False):

    episode_cnt = 0

    if eval:
        ql.load(os.path.join(run_dir, ACTION_STATE_VALUES_FN))

        # Keep track of total pucks caught and average number of steps taken
        puck_count = 0
        eval_steps = []
    elif resume:
        ql.load(os.path.join(run_dir, ACTION_STATE_VALUES_FN))
        log_f1.seek(0)
        episode_cnt = sum(1 for line in log_f)

    for e in ql.episode_iterator(start=episode_cnt):

        # Keep track of episode's rewards and visited states for logging
        rewards = []
        states = []

        state = ql.get_state()
        states.append(state)

        for s in ql.step_iterator():
            action = ql.get_action(state)
            reward, new_state = ql.take_action(action)

            # Store logging info
            rewards.append(reward)
            states.append(new_state)

            # Only perform an update if we're training
            if new_state == -1:
                if not eval:
                    ql.update_value(state, action, -100000, state)
                break
            if not eval:
                ql.update_value(state, action, reward, new_state)

            state = new_state

            # In case we found goal, break
            if reward == config['goal_reward']:
                if eval:
                    puck_count += 1
                    eval_steps.append(s+1)
                break

        print(f"\rFinished episode {e+1} (total of {s+1} steps) (return: {sum(rewards):.6f})")

        # Logs
        if not eval:
            for r in rewards:
                log_f1.write(f"{r:.3f} ")
            log_f1.write("\n")
            for s in states:
                log_f2.write(f"{s} ")
            log_f2.write("\n")

        robotino.stop_sim()

    if eval:
        log_f1.write(f"{puck_count}/{ql.total_episodes}\n") # Puck count / total episodes
        log_f1.write(f"{np.mean(eval_steps)}\n") # Mean number of steps per succesfull episode
        log_f1.write(f"{np.std(eval_steps)}\n") # Standard deviation of number of steps per succesfull episode
        log_f1.write(f"{1.96*np.std(eval_steps)}\n") # 95% confidence interval of number of steps per succesfull episode

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output_dir", type=str, default='bin')
    parser.add_argument("-e", "--experiment_dir", type=str, default='experiments')
    parser.add_argument("--eval", type=int, default=0)
    parser.add_argument("--resume", type=int, default=0)
    parser.add_argument("--show", action='store_true')
    args = parser.parse_args()

    assert not (args.eval and args.resume)

    if args.eval:
        config['steps'] = 1000
        config['episodes'] = 5
        config['policy'] = 'eps-greedy'
        config['epsilon'] = [0.0, 0.0]

    robotino = robot.Robotino(args.output_dir, args.eval, radius=1.5)
    ql = qlearn.QLearn(robotino, config)

    # Find a unique id for run
    if args.eval:
        run_dir = os.path.join(args.experiment_dir, f"run-{args.eval}")
    elif args.resume:
        run_dir = os.path.join(args.experiment_dir, f"run-{args.resume}")
    else:
        runs = glob.glob(os.path.join(args.experiment_dir, "*"))
        id = 1
        if runs:
            run_ids = [int(run_dir.split('-')[-1]) for run_dir in runs]
            id = sorted(run_ids)[-1] + 1

        # Create a folder for this run outputs
        run_dir = os.path.join(args.experiment_dir, f"run-{id}")
        os.mkdir(run_dir)

        # Write current git HEAD's SHA to log dir
        with open(os.path.join(run_dir, "commit-sha.txt"), "w") as f:
            sha = subprocess.check_output(["git", "rev-parse", "HEAD"]).strip()
            f.write(sha.decode('ascii')+"\n")

        # Write config dict to file
        with open(os.path.join(run_dir, "config.txt"), "w") as f:
            f.write(str(config) + "\n")
            f.write(str(robotino.resolution) + "\n")

    # Open log file to keep track of per-step rewards of all episodes
    if args.eval:
        eval_fn = os.path.join(run_dir, "eval.txt")
        log_f1 = open(eval_fn, "a+")
        log_f2 = None
    else:
        rewards_fn = os.path.join(run_dir, "rewards.txt")
        states_fn = os.path.join(run_dir, "states.txt")
        log_f1 = open(rewards_fn, "a+")
        log_f2 = open(states_fn, "a+")

    try:
        main(robotino, ql, log_f1, log_f2, run_dir, args.eval, args.resume, args.show)
    except Exception as e:
        traceback.print_exc()
    finally:
        if not args.eval:
            ql.save(os.path.join(run_dir, ACTION_STATE_VALUES_FN))
            log_f2.close()
        log_f1.close()
        robotino.stop_sim()
        cv2.destroyAllWindows()
