import os
import sys
import argparse

import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt
import matplotlib.colors as colors

action_to_arrow = {
    0: {'x': 0.5, 'y': 0.8, 'dx': 0.0, 'dy': -0.6},
    1: {'x': 0.5, 'y': 0.2, 'dx': 0.0, 'dy': 0.6},
    2: {'x': 0.8, 'y': 0.5, 'dx': -0.6, 'dy': 0.0},
    3: {'x': 0.2, 'y': 0.5, 'dx': 0.6, 'dy': 0.0},

    4: {'x': 0.8, 'y': 0.8, 'dx': -0.6, 'dy': -0.6},
    5: {'x': 0.8, 'y': 0.2, 'dx': -0.6, 'dy': 0.6},
    6: {'x': 0.8, 'y': 0.2, 'dx': -0.6, 'dy': 0.6},
    7: {'x': 0.2, 'y': 0.8, 'dx': 0.6, 'dy': -0.6},

    8: {'x': 0.2, 'y': 0.8, 'dx': 0.6, 'dy': -0.6},
    9: {'x': 0.2, 'y': 0.2, 'dx': 0.6, 'dy': 0.6},
    10: {'x': 0.8, 'y': 0.8, 'dx': -0.6, 'dy': -0.6},
    11: {'x': 0.2, 'y': 0.2, 'dx': 0.6, 'dy': 0.6},
}

# action_to_vel = {
#     0: [MAX_LINEAR, 0.0,  0.0], # Move up, no steering
#     1: [-MAX_LINEAR, 0.0, 0.0], # Move down, no steering
#     2: [0.0, MAX_LINEAR, 0.0], # Move left, no steering
#     3: [0.0, -MAX_LINEAR, 0.0], # Move right, no steering
#     4: [MAX_LINEAR, 0.0,  MAX_ANGULAR], # Move up, steer left
#     5: [-MAX_LINEAR, 0.0, MAX_ANGULAR], # Move down, steer left
#     6: [0.0, MAX_LINEAR, MAX_ANGULAR], # Move left, steer left
#     7: [0.0, -MAX_LINEAR, MAX_ANGULAR], # Move right, steer left
#     8: [MAX_LINEAR, 0.0,  -MAX_ANGULAR], # Move up, steer right
#     9: [-MAX_LINEAR, 0.0, -MAX_ANGULAR], # Move down, steer right
#     10: [0.0, MAX_LINEAR, -MAX_ANGULAR], # Move left, steer right
#     11: [0.0, -MAX_LINEAR, -MAX_ANGULAR], # Move right, steer right
# }


def main(id, exp_dir, episodes, granularity):
    """Plots the graphs (return vs episode) and (steps vs episodes)
    Args:
        id: Run id.
        file: File that contains the logs.
        episodes: Number of episodes to parse.
        granularity: Size of chunk to use when calculating metrics.
    """

    run_dir = os.path.join(exp_dir, f"run-{id}")
    reward_fn = os.path.join(run_dir, "rewards.txt")
    state_fn = os.path.join(run_dir, "states.txt")
    config_fn = os.path.join(run_dir, "config.txt")
    qvalue_fn = os.path.join(run_dir, "action-state-values.npy")

    # Find out goal reward from config file
    goal_reward = None
    width, height = None, None
    with open(config_fn, "r") as f:
        config = eval(f.readline())
        width, height = eval(f.readline())
        goal_reward = config['goal_reward']
        actions = config['actions']

    returns = []
    steps = []
    steps_xs = []
    with open(reward_fn, "r") as f:
        for i, line in enumerate(f):
            if i == episodes:
                break
            rewards = list(map(float, line.split()))

            # Always remove last reward since it doesnt represent how good a run was
            last_reward = rewards.pop()
            if last_reward == goal_reward:
                steps.append(len(rewards) + 1) # Only include runs that succeeded
                steps_xs.append(i+1)
            returns.append(sum(rewards))
        if episodes == -1:
            episodes = i + 1

    returns_std = []
    returns_mean = []

    if granularity > 1:
        for i in range(0, episodes, granularity):
            rets = returns[i:i + granularity]
            returns_mean.append(stats.tmean(rets))
            returns_std.append(1.96 * stats.tstd(rets))
    else:
        returns_mean = returns
        returns_std = np.zeros(len(returns), dtype=float)

    # Plotting returns vs episodes
    fig, ax = plt.subplots()
    x = range(1, episodes, granularity)
    ax.scatter(x, returns_mean, s=4.5, color="b")
    ax.errorbar(x, returns_mean, returns_std, linestyle='None', color='r', alpha=0.3)
    ax.set_xlim(0, 1.1 * max(x))
    ax.set_xlabel("Episódio")
    ax.set_ylabel("$G_0$")
    ax.axis("auto")
    fig.savefig(os.path.join(run_dir, "returns.pdf"))

    # Plotting steps vs episodes
    if steps:
        fig, ax = plt.subplots()
        x = range(1, len(steps) + 1)
        ax.scatter(steps_xs, steps, s=4.5, color="b")
        ax.set_xlim(0, 1.1 * max(steps))
        ax.set_xlabel("Episódio")
        ax.set_ylabel("Passos")
        ax.axis("auto")
        fig.savefig(os.path.join(run_dir, "steps.pdf"))

    states_map = np.zeros((height, width), dtype=np.int64)
    with open(state_fn, "r") as f:
        for line in f:
            states = list(map(int, line.split()))
            if states[-1] == -1:
                states.pop()
            for s in states:
                h = s // width
                w = s % width
                states_map[h, w] += 1
    states_map = np.log(states_map + 0.1)
    fig, ax = plt.subplots()
    ax.imshow(states_map, cmap='inferno')
    fig.savefig(os.path.join(run_dir, "states-map.pdf"))

    if actions == 'pid' and os.path.exists(qvalue_fn):
        Q = np.load(qvalue_fn)
        Q = np.argmax(Q, axis=1).reshape(height, width)
        ax.imshow(np.zeros((height+1, width+1)))
        ax.set_xlim(0, width)
        ax.set_ylim(height, 0)
        for (i, j), _ in np.ndenumerate(Q):
            x, y, dx, dy = action_to_arrow[Q[i, j]].values()
            ax.arrow(
                x=j+x, y=i+y, dx=dx, dy=dy,
                length_includes_head=True,
                head_width=0.1,
                head_length=0.1,
                color='r'
            )
        fig.savefig(os.path.join(run_dir, "actions-map.pdf"))


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--id", type=int, required=True)
    parser.add_argument("--exp_dir", type=str, required=True)
    parser.add_argument("-e", "--episodes", type=int, default=-1)
    parser.add_argument("-g", "--granularity", type=int, default=50)
    args = parser.parse_args()

    if args.episodes != -1 and args.episodes % args.granularity:
        raise ValueError("Number of episodes must be divisible by granularity!!!")

    main(**vars(args))
