import sys
import math
import os

import cv2
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import UnivariateSpline

# Color map for terminal colors
color_to_code = {
    'g': 34,
    'r': 1,
    'y': 3
}

# Color to HSV range map
color_to_range = {
    'y': {'upper': np.array([40, 270, 281]), 'lower': np.array([20, 200, 201])},
    'g': {'upper': np.array([71, 180, 270]), 'lower': np.array([51, 110, 200])},
    'b': {'upper': np.array([110, 255, 255]), 'lower': np.array([100, 50, 50])},
}

def cprint(text, color, **kwargs):
    """Wrapper to print colored text

    Args:
        text: String that should be printed to stdout
        color: Desired color for the string
    """
    code = color_to_code[color]
    text = f"\033[38;5;{code}m{text}\033[0m"

    if color == 'r':
        sys.exit(text)
    else:
        print(text, **kwargs)

def vrep_to_array(image, resolution):
    """Converts vrep image to a numpy BGR array.

    Args:
        image: List with raw pixel data received from vrep image sensor.
        resolution: Tuple with desired image resolution to create numpy array.

    Returns:
        img: Image as a numpy array in BGR.

    """
    img = np.array(image).astype(np.uint8)
    img = np.reshape(img, (resolution[1], resolution[0], -1))
    img = cv2.flip(img, 0)  # Flip image over x-axis
    img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    return img

def get_mask(image, color='b'):
    """Retrieve a binary mask where whites (255) represent the foreground and
    black (0) the background.

    Args:
        image: Input image.
        color: The color that should be considered a foreground object.
    Returns:
        mask: Binary output image of the same size of input with white pixels
        where the interval specified by `color` is respected.
    """
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    upper = color_to_range[color]['upper']
    lower = color_to_range[color]['lower']
    mask = cv2.inRange(hsv, lower, upper)
    return mask


def get_centroid_coord(mask):
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (6, 6))
    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)

    # Find all contours
    _, contours, _ = cv2.findContours(mask, mode=cv2.RETR_LIST, method=cv2.CHAIN_APPROX_SIMPLE)

    # In case there are no contours
    if not contours:
        return None

    # Sort contours by area
    contours = [(cnt, cv2.contourArea(cnt)) for cnt in contours]
    contours = sorted(contours, key=lambda cnt: cnt[1])

    # Compute centroid for largest contour
    M = cv2.moments(contours[-1][0])

    # Prevent division by zero
    if M['m00'] == 0:
        return None

    cx = int(M['m10']/M['m00'])
    cy = int(M['m01']/M['m00'])

    return cx, cy


def test_mask(img, color='b'):
    """Get a mask of a image (filtering by a specific color), find the centroid
        of the feature and print them.
    Args:
        image: Input image.
        color: The color that should be considered a foreground object.
    """
    mask = get_mask(img, color)
    cv2.imshow("Vision Sensor w Mask", mask)
    cv2.waitKey(1)
    cx, cy = get_centroid_dist(mask)
    print(cx, cy)
