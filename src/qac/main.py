import os
import glob
import argparse
import traceback
import subprocess

import cv2
import tensorflow as tf

import qac
import robot
import utils

config = {
    "gamma": 0.99,
    "steps": 1000,
    "episodes": 2000,
    "goal": (0.5125, 0.95), # (x, y) normalized coordinates for goal
    "terminate_reward": -100000,
    "goal_reward": 100,
    "policy_lr": 0.01,
    "qvalue_lr": 0.01
}

def main(robotino, qactor, log_f, run_dir, eval, resume, show=False):

    episode_cnt = 0

    returns = []
    steps = []
    for e in qactor.episode_iterator(start=episode_cnt):

        rewards = []                # save this episode's logs
        reward_acc = 0              # save this episode's sum of rewards

        state, _ = qactor.get_state()
        action = qactor.get_sample_action(state)
        qvalue = qactor.compute_qvalue(state, action)

        for s in qactor.step_iterator():
            # Find out qvalue for next state-action pair
            reward, new_state = qactor.take_action(action)

            rewards.append(reward)

            new_action = qactor.get_sample_action(new_state)
            new_qvalue = qactor.compute_qvalue(new_state, new_action)

            # Perform actor and critic updates
            qactor.update_policy(state, qvalue)
            qactor.update_qvalue(state, action, qvalue, new_qvalue, reward)

            # In case we loose the puck or finish grabbing it
            if reward == config['terminate_reward'] or reward == config['goal_reward']:
                break

            action = new_action
            state = new_state

            reward_acc += reward

        # Logs
        if not eval:
            for r in rewards:
                log_f.write(f"{r:.3f} ")
            log_f.write("\n")

        # Metrics
        qactor.log_scalar("return", reward_acc, e+1)
        qactor.log_scalar("nr_steps", s+1, e+1)
        robotino.stop_sim()

    print(f"\rFinished episode {e+1} (total of {s+1} steps) (return: {reward_acc})")
    utils.my_plot(returns, steps, run_dir)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output_dir", type=str, default='bin')
    parser.add_argument("-e", "--experiment_dir", type=str, default='experiments')
    parser.add_argument("--eval", type=int, default=0)
    parser.add_argument("--resume", type=int, default=0)
    parser.add_argument("--show", action='store_true')
    args = parser.parse_args()

    assert not (args.eval and args.resume)

    # Find a unique id for run
    if args.eval:
        run_dir = os.path.join(args.experiment_dir, f"run-{args.eval}")
    elif args.resume:
        run_dir = os.path.join(args.experiment_dir, f"run-{args.resume}")
    else:
        runs = glob.glob(os.path.join(args.experiment_dir, "*"))
        id = 1
        if runs:
            run_ids = [int(run_dir.split('-')[-1]) for run_dir in runs]
            id = sorted(run_ids)[-1] + 1

        # Create a folder for this run outputs
        run_dir = os.path.join(args.experiment_dir, f"run-{id}")
        os.mkdir(run_dir)

        # Write current git HEAD's SHA to log dir
        with open(os.path.join(run_dir, "commit-sha.txt"), "w") as f:
            sha = subprocess.check_output(["git", "rev-parse", "HEAD"]).strip()
            f.write(sha.decode('ascii')+"\n")

        # Write config dict to file
        with open(os.path.join(run_dir, "config.txt"), "w") as f:
            f.write(str(config))

    # Open log file to keep track of per-step rewards of all episodes
    if args.eval:
        log_fn = os.path.join(run_dir, "eval.txt")
    else:
        log_fn = os.path.join(run_dir, "rewards.txt")
    log_f = open(log_fn, "a+")

    session = tf.Session()
    robotino = robot.Robotino(args.output_dir)
    qactor = qac.QActorCritic(session, robotino, config, run_dir)

    try:
        main(robotino, qactor, log_f, run_dir, args.eval, args.resume, args.show)
    except Exception as e:
        traceback.print_exc()
    finally:
        log_f.close()
        session.close()
        robotino.stop_sim()
