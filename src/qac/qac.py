import cv2
import numpy as np
import tensorflow as tf

import time
import utils

MAX_VELOCITY = 1.0

action_to_rot = {
    0: -0.5,
    1: 0.0,
    2: 0.5
}

class QActorCritic:
    def __init__(self, sess, robotino, config, run_dir):
        self.sess = sess
        self.robotino = robotino

        self.gamma = config['gamma']
        self.policy_lr = config['policy_lr']
        self.qvalue_lr = config['qvalue_lr']

        # Helper constants
        self.width, self.height = robotino.resolution
        self.diag = np.sqrt(self.width**2 + self.height**2)

        # Keep track of current step and episode
        self.step = 0
        self.episode = 0
        self.total_episodes = config['episodes']
        self.total_steps = config['steps']

        # Scale goal to image resolution
        self.goal_coord = config['goal'][0]*self.width, config['goal'][1]*self.height
        self.goal_reward = config['goal_reward']
        self.terminate_reward = config['terminate_reward']

        # Define placholders to update weights
        self.state_value = tf.placeholder(
            dtype=tf.float32,
            shape=(),
            name="qvalue_state"
        )
        self.new_state_value = tf.placeholder(
            dtype=tf.float32,
            shape=(),
            name="qvalue_new_state"
        )
        self.reward = tf.placeholder(
            dtype=tf.float32,
            shape=(),
            name="reward"
        )

        with tf.variable_scope("policy"):
            self.policy_state = tf.placeholder(
                dtype=tf.float32,
                shape=(None, self.height, self.width, 1),
                name="policy_state")
            self.policy_actions = self.policy_net(self.policy_state)
            self.policy_actions_prob = tf.nn.softmax(self.policy_actions, name="action_probabilities")
            self.sample_action = tf.math.argmax(self.policy_actions_prob, axis=1)
            print("Sample action shape", self.sample_action.shape)

            self.policy_loss = -tf.reduce_mean(tf.log(self.policy_actions_prob))
            self.policy_step_size = self.policy_lr * self.state_value

            policy_optimizer = tf.train.GradientDescentOptimizer(self.policy_step_size)
            policy_gradients = policy_optimizer.compute_gradients(self.policy_loss)
            self.policy_update_op = policy_optimizer.apply_gradients(policy_gradients)

        with tf.variable_scope("qvalue"):
            self.qvalue_state = tf.placeholder(
                dtype=tf.float32,
                shape=(None, self.height, self.width, 1),
                name="qvalue_state")
            self.qvalue_action = tf.placeholder(
                dtype=tf.float32,
                shape=(None, 1),
                name="qvalue_action")
            self.qvalue = self.qvalue_net(self.qvalue_state, self.qvalue_action)

            self.qvalue_loss = -self.qvalue
            self.qvalue_step_size = \
                self.qvalue_lr * (self.reward + self.gamma*self.new_state_value - self.state_value)

            qvalue_optimizer = tf.train.GradientDescentOptimizer(self.qvalue_step_size)
            qvalue_gradients = qvalue_optimizer.compute_gradients(self.qvalue_loss)
            self.qvalue_update_op = qvalue_optimizer.apply_gradients(qvalue_gradients)

        # Create summary writer object to log loss summaries and graph data
        self.summary_writer = tf.summary.FileWriter(run_dir, self.sess.graph)

        self.sess.run(tf.global_variables_initializer())

    def compute_qvalue(self, state, action):
        action = action.reshape(1, 1)
        qvalue = self.sess.run(self.qvalue,
            feed_dict={
                self.qvalue_state:state,
                self.qvalue_action:action
            })
        return qvalue

    def update_qvalue(self, state, action, qvalue, new_qvalue, reward):
        self.sess.run(self.qvalue_update_op,
            feed_dict={
                self.qvalue_state: state,
                self.qvalue_action: action.reshape((1,1)),
                self.state_value: qvalue.reshape(()),
                self.new_state_value: new_qvalue.reshape(()),
                self.reward: reward
            })

    def update_policy(self, state, qvalue_state):
        self.sess.run(self.policy_update_op,
            feed_dict={
                self.policy_state: state,
                self.state_value: qvalue_state.reshape(())
            })

    def get_sample_action(self, state):
        action = self.sess.run(self.sample_action,
            feed_dict={
                self.policy_state:state
            })
        return action

    def take_action(self, action):
        qt, w1 = divmod(action[0], 3)
        w2, w3 = divmod(qt, 3)
        self.robotino.set_wheel_velocity(
            wheel_vels=[
                action_to_rot[w1],
                action_to_rot[w2],
                action_to_rot[w3]
            ]
        )
        self.robotino.wait(10)
        new_state, new_centroid = self.get_state()

        if new_centroid is None:
            return self.terminate_reward, new_state

        # Give a positive reward for moving in the direction of the puck
        dist = self.distance(new_centroid, self.goal_coord)/self.diag
        if dist < 0.03:
            reward = self.goal_reward
        else:
            reward = self.prev_dist - dist
        self.prev_dist = dist
        return reward, new_state

    def get_state(self):
        img = self.robotino.read_vision_sensor()
        mask = utils.get_mask(img, color='b')
        centroid = utils.get_centroid_coord(mask)

        state = mask.reshape(1, mask.shape[0], mask.shape[1], 1)

        # In case puck is out of range
        if centroid is None:
            return state, None

        return state, centroid

    def qvalue_net(self, state, action):
        conv1 = self.conv2d(state,
            filters=2,
            strides=[2,2],
            kernel_size=[3,3],
            padding="same",
            kernel_initializer=tf.initializers.glorot_uniform,
            bias_initializer=tf.initializers.zeros,
            name="conv1")
        maxpool1 = self.maxpool2d(conv1,
            ksize=[2,2],
            strides=[2,2],
            padding='same',
            name='pool1')
        conv2 = self.conv2d(maxpool1,
            filters=4,
            strides=[2,2],
            kernel_size=[3,3],
            padding="same",
            kernel_initializer=tf.initializers.glorot_uniform,
            bias_initializer=tf.initializers.zeros,
            name="conv2")
        maxpool2 = self.maxpool2d(conv2,
            ksize=[2,2],
            strides=[2,2],
            padding='same',
            name='pool2')
        conv3 = self.conv2d(maxpool2,
            filters=1,
            strides=[2,2],
            kernel_size=[3,3],
            padding="same",
            kernel_initializer=tf.initializers.glorot_uniform,
            bias_initializer=tf.initializers.zeros,
            name="conv3")
        flat = tf.layers.flatten(conv3)
        fc_input = tf.concat([flat, action], axis=1)
        fc1 = tf.layers.dense(inputs=fc_input,
            units=128,
            bias_initializer=tf.initializers.constant(1.0),
            kernel_initializer=tf.initializers.random_normal(stddev=0.001),
            activation=tf.nn.relu,
            name="fc1")
        qvalue = tf.layers.dense(inputs=fc1,
            name="qvalue",
            units=1,
            bias_initializer=tf.initializers.constant(1.0),
            kernel_initializer=tf.initializers.random_normal(stddev=0.001),
            activation=None)
        return qvalue

    def policy_net(self, state):
        conv1 = self.conv2d(state,
            filters=2,
            kernel_size=[3,3],
            strides=[2,2],
            padding="same",
            kernel_initializer=tf.initializers.glorot_uniform,
            bias_initializer=tf.initializers.zeros,
            name="conv1")
        maxpool1 = self.maxpool2d(conv1,
            ksize=[2,2],
            strides=[2,2],
            padding='same',
            name='pool1')
        conv2 = self.conv2d(maxpool1,
            filters=4,
            kernel_size=[3,3],
            strides=[2,2],
            padding="same",
            kernel_initializer=tf.initializers.glorot_uniform,
            bias_initializer=tf.initializers.zeros,
            name="conv2")
        maxpool2 = self.maxpool2d(conv2,
            ksize=[2,2],
            strides=[2,2],
            padding='same',
            name='pool2')
        conv3 = self.conv2d(maxpool2,
            filters=1,
            kernel_size=[3,3],
            strides=[2,2],
            padding="same",
            kernel_initializer=tf.initializers.glorot_uniform,
            bias_initializer=tf.initializers.zeros,
            name="conv3")
        flat = tf.layers.flatten(conv3)
        fc1 = tf.layers.dense(inputs=flat,
            units=128,
            bias_initializer=tf.initializers.constant(1.0),
            kernel_initializer=tf.initializers.random_normal(stddev=0.001),
            activation=tf.nn.relu,
            name="fc1")
        actions = tf.layers.dense(inputs=fc1,
            name="actions",
            units=27,
            bias_initializer=tf.initializers.constant(1.0),
            kernel_initializer=tf.initializers.random_normal(stddev=0.001),
            activation=tf.nn.sigmoid)
        return actions

    def episode_iterator(self, start=0):
        self.episode = start
        self.robotino.start_sim()
        state = -1 # TODO: Fix
        centroid = self.state_to_coord(state)
        self.prev_dist = self.distance(centroid, self.goal_coord)/self.diag
        while self.episode < self.total_episodes:
            yield self.episode
            self.episode += 1
            self.step = 0
            self.robotino.start_sim()
            state = -1 # TODO: Fix
            centroid = self.state_to_coord(state)
            self.prev_dist = self.distance(centroid, self.goal_coord)/self.diag

    def step_iterator(self):
        while self.step < self.total_steps:
            yield self.step
            self.step += 1

    def distance(self, point1, point2):
        return np.sqrt((point2[0]-point1[0])**2 + (point2[1]-point1[1])**2)

    def maxpool2d(self, inputs, ksize, strides, padding, name=None):
        return tf.nn.max_pool(
            value=inputs,
            ksize=[1, ksize[0], ksize[1], 1],
            strides=[1, strides[0], strides[1], 1],
            padding=padding.upper(),
            name=name)

    def conv2d(self, inputs, filters, kernel_size,
               strides, padding,
               kernel_initializer, bias_initializer, transpose=False,
               kernel_regularizer=None, bias_regularizer=None,
               activation=tf.nn.relu, name=None):

        # Set kernel shape to None if initializer is a constant due to tf ValueError
        if type(kernel_initializer) is np.ndarray:
            kernel_shape = None
        else:
            if transpose:
                kernel_shape = (kernel_size[0], kernel_size[1], filters, inputs.shape[-1])
            else:
                kernel_shape = (kernel_size[0], kernel_size[1], inputs.shape[-1], filters)

        if type(bias_initializer) is np.ndarray:
            bias_shape = None
        else:
            bias_shape = (filters,)

        with tf.variable_scope(name):
            kernel = tf.get_variable(
                name="kernel",
                shape=kernel_shape,
                dtype=tf.float32,
                initializer=kernel_initializer,
                regularizer=kernel_regularizer,
                trainable=True,
            )
            bias = tf.get_variable(
                name="bias",
                shape=bias_shape,
                dtype=tf.float32,
                initializer=bias_initializer,
                regularizer=bias_regularizer,
                trainable=True
            )
            strides = [1, strides[0], strides[1], 1]
            if transpose:
                # Desired tensor output shape
                output_shape = [
                    tf.shape(inputs)[0],
                    strides[1]*inputs.shape[1],
                    strides[2]*inputs.shape[2],
                    filters
                ]
                conv = tf.nn.conv2d_transpose(inputs, kernel, output_shape, strides, padding.upper())
            else:
                conv = tf.nn.conv2d(inputs, kernel, strides, padding.upper())
            out = activation(tf.nn.bias_add(conv, bias))
            return out

    def coord_to_state(self, coord):
        x, y = coord
        return y * self.width + x

    def state_to_coord(self, state):
        y = state // self.width
        x = state % self.width
        return x, y

    def log_scalar(self, tag, value, step):
        summary = tf.Summary(value=[tf.Summary.Value(tag=tag, simple_value=value)])
        self.summary_writer.add_summary(summary, step)

    # def load(self, filename):
    #     self.Q = np.load(filename)
    #
    # def save(self, filename):
    #     return np.save(filename, self.Q)
