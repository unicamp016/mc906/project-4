import os
import sys, time

import cv2
import numpy as np

import vrep
import utils

class Robotino():
	def __init__(self, output_dir, eval=False, radius=1.5):
		self.output_dir = output_dir
		self.eval = eval
		self.radius = radius
		self.SERVER_IP = "127.0.0.1"
		self.SERVER_PORT = 19997
		self.clientID = self.init_sim()
		self.robot_handle, self.puck_handle = self.init_robot()
		self.vision_handle, self.resolution = self.init_vision_sensor()
		self.wheel_handles = self.init_wheels()
		self.init_pos = self.get_current_position()
		self.init_ori = self.get_current_orientation()
		self.alpha = np.pi/2
		self.beta = np.pi/6

		# Make sure first episode starts normally
		self.start_sim()
		self.stop_sim()

	def init_sim(self):
		"""Function to start the simulation. The scene must be running before
		running this code.

		Returns:
			clientID: This ID is used to start the objects on the scene.
		"""
		vrep.simxFinish(-1)
		clientID = vrep.simxStart(self.SERVER_IP, self.SERVER_PORT, True, True, 2000, 5)
		vrep.simxSynchronous(clientID, enable=True)
		if clientID != -1:
			utils.cprint("Connected to remoteApi server.", "g")
		else:
			vrep.simxFinish(clientID)
			utils.cprint("ERROR: Unable to connect to remoteApi server."
			"Consider running scene before executing script.", "r")
		return clientID

	def init_vision_sensor(self):
		"""Function to start the sensors.

		Returns:
			vision_handle: Contains the vision sensor handle ID.
		"""

		# Starting vision sensor
		res, vision_handle = vrep.simxGetObjectHandle(
			clientID=self.clientID,
			objectName="Vision_sensor",
			operationMode=vrep.simx_opmode_oneshot_wait
		)
		if res != vrep.simx_return_ok:
			utils.cprint("Vision sensor not connected.", "r")
		else:
			utils.cprint("Vision sensor connected.", "g")

		res, resolution, _ = vrep.simxGetVisionSensorImage(
			clientID=self.clientID,
			sensorHandle=vision_handle,
			options=0,
			operationMode=vrep.simx_opmode_blocking
		)

		if res != vrep.simx_return_ok:
			utils.cprint("Unable to retrieve vision sensor data", "r")

		# Start streaming vision sensor data
		vrep.simxGetVisionSensorImage(
			clientID=self.clientID,
			sensorHandle=vision_handle,
			options=0,
			operationMode=vrep.simx_opmode_streaming
		)

		return vision_handle, tuple(resolution)

	def init_robot(self):
		"""Function to start the robot.

		Returns:
			robot_handle: Contains the robot handle ID.
		"""
		res, robot_handle = vrep.simxGetObjectHandle(
			clientID=self.clientID,
			objectName="robotino",
		 	operationMode=vrep.simx_opmode_blocking
		)
		if res != vrep.simx_return_ok:
			utils.cprint("Robotino not connected.", "r")
		else:
			utils.cprint("Robotino connected.", "g")

		res, puck_handle = vrep.simxGetObjectHandle(
			clientID=self.clientID,
			objectName="Puck_Azul",
		 	operationMode=vrep.simx_opmode_blocking
		)
		if res != vrep.simx_return_ok:
			utils.cprint("Couldn't get puck handle.", "r")
		else:
			utils.cprint("Got puck handle.", "g")

		vrep.simxGetObjectPosition(self.clientID, robot_handle, -1,
								   vrep.simx_opmode_streaming)
		vrep.simxGetObjectOrientation(self.clientID, robot_handle, -1,
									  vrep.simx_opmode_streaming)

		return robot_handle, puck_handle

	def init_wheels(self):
		"""Function to get the wheel handles.

		Returns:
			wheel_handles: List of wheel handles.
		"""
		wheel_handles = []
		for i in range(3):
			res, wheel_handle = vrep.simxGetObjectHandle(
				clientID=self.clientID,
				objectName=f"wheel{i}_joint",
			 	operationMode=vrep.simx_opmode_blocking
			)
			wheel_handles.append(wheel_handle)

			if res != vrep.simx_return_ok:
				utils.cprint(f"Couldn't get wheel joint {i}", "r")
			else:
				utils.cprint(f"\rRetrieved wheel joint {i+1}/3", "y", end="")

		utils.cprint("\rRetrieved all wheel joints" + " "*10, "g")
		return wheel_handles

	def start_sim(self, random=False):
		"""Start running simulation.

		Returns:
			return_code: A remote API function return code
		"""
		if eval:
			self.set_init_pos(random=True)
		ret = vrep.simxStartSimulation(self.clientID, vrep.simx_opmode_blocking)
		self.wait(5)
		return ret

	def stop_sim(self):
		"""Stops current running simulation.

		Returns:
			return_code: A remote API function return code
		"""
		ret = vrep.simxStopSimulation(self.clientID, vrep.simx_opmode_blocking)
		self.reset_init_pos()
		time.sleep(1)
		return ret

	def set_velocity(self, linear_x, linear_y, angular_z):
		"""Set robot velocity.

		Args:
			linear_x: Positive means right, negative means left
			linear_y: Positive means forward, negative means backwards
			angular_z: Positve means turn right, negative means turn left
		"""
		return vrep.simxCallScriptFunction(self.clientID, "robotino",
			vrep.sim_scripttype_childscript,
			"twistCallback",
			[],
			[linear_x, linear_y, angular_z],
			[],
			bytearray(),
			vrep.simx_opmode_blocking
		)

	def set_wheel_velocity(self, wheel_vels):
		"""Set robot wheel velocities.

		Args:
			wheel_vels: Wheel velocities
		"""
		for wh, wv in zip(self.wheel_handles, wheel_vels):
			vrep.simxSetJointTargetVelocity(
				self.clientID,
				jointHandle=wh,
				targetVelocity=wv,
				operationMode=vrep.simx_opmode_oneshot
			)


	def get_connection_status(self):
		"""Function to inform if the connection with the server is active.

		Returns:
			connectionId: -1 if the client is not connected to the server.
			Different connection IDs indicate temporary disconections in-between.
		"""
		return vrep.simxGetConnectionId(self.clientID)

	def read_vision_sensor(self):
		"""Reads the image raw data from vrep vision sensor.

		Returns:
			image: A BGR numpy array with the raw image from the sensor.
		"""

		while True:
			res, resolution, image = vrep.simxGetVisionSensorImage(
				clientID=self.clientID,
				sensorHandle=self.vision_handle,
				options=0,
				operationMode=vrep.simx_opmode_buffer
			)
			if res == vrep.simx_return_ok:
				break
		image = utils.vrep_to_array(image, resolution)
		return image

	def get_current_position(self):
		"""Gives the current robot position on the environment.

		Returns:
			position: Array with the (x,y,z) coordinates.
		"""
		while True:
			res, position = vrep.simxGetObjectPosition(
				clientID=self.clientID,
				objectHandle=self.robot_handle,
				relativeToObjectHandle=-1,
				operationMode=vrep.simx_opmode_buffer
			)
			if res == vrep.simx_return_ok:
				break
		return position

	def get_current_orientation(self):
		"""Gives the drone's current absolute orientation.

		Returns:
			orientation: Array with the euler angles (alpha, beta and gamma).
		"""

		while True:
			res, orientation = vrep.simxGetObjectOrientation(
				clientID=self.clientID,
				objectHandle=self.robot_handle,
				relativeToObjectHandle=-1,
				operationMode=vrep.simx_opmode_buffer
			)
			if res == vrep.simx_return_ok:
				break

		return orientation

	def set_init_pos(self, random):

		if random:
			alpha = np.random.uniform(0, 2*np.pi)
			beta = np.random.uniform(-np.pi/8, np.pi/8)
		else:
			alpha = self.alpha
			beta = self.beta

		vrep.simxSetObjectPosition(self.clientID,
			objectHandle=self.robot_handle,
			relativeToObjectHandle=self.puck_handle,
			position=[self.radius*np.cos(alpha), self.radius*np.sin(alpha), 0],
			operationMode=vrep.simx_opmode_oneshot
		)
		vrep.simxSetObjectOrientation(self.clientID,
			objectHandle=self.robot_handle,
			relativeToObjectHandle=self.robot_handle,
			eulerAngles=[0, 0, np.pi+alpha+beta],
			operationMode=vrep.simx_opmode_oneshot
		)

	def reset_init_pos(self):
		vrep.simxSetObjectPosition(self.clientID,
			objectHandle=self.robot_handle,
			relativeToObjectHandle=-1,
			position=self.init_pos,
			operationMode=vrep.simx_opmode_blocking
		)
		vrep.simxSetObjectOrientation(self.clientID,
			objectHandle=self.robot_handle,
			relativeToObjectHandle=-1,
			eulerAngles=self.init_ori,
			operationMode=vrep.simx_opmode_blocking
		)

	def wait(self, steps):
		for _ in range(steps):
			vrep.simxSynchronousTrigger(self.clientID)
		vrep.simxGetPingTime(self.clientID)

	def time(self):
		return vrep.simxGetLastCmdTime(self.clientID)
